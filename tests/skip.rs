extern crate transformation_pipeline;

use transformation_pipeline::{TransformationStage, StageActions, StageResult, TransformationPipeline};

struct FirstTransformation {}

impl TransformationStage<String> for FirstTransformation {

    fn run(&self, _previous: String) -> StageResult<String> {
        Ok(StageActions::Next("a".to_owned()))
    }
}

struct SecondTransformation {}

impl TransformationStage<String> for SecondTransformation {

    fn run(&self, _previous: String) -> StageResult<String> {
        Ok(StageActions::Skip)
    }

}

struct ThirdTransformation {}

impl TransformationStage<String> for ThirdTransformation {

    fn run(&self, previous: String) -> StageResult<String> {
        assert_eq!(previous, "a".to_owned());
        Ok(StageActions::Next("b".to_owned()))
    }

}

#[test]
fn run_pipeline() {
    let pipeline: TransformationPipeline<String> = TransformationPipeline::new(vec![
        Box::new(FirstTransformation {}),
        Box::new(SecondTransformation {}),
        Box::new(ThirdTransformation {}),
    ]);

    assert_eq!(pipeline.run("z".to_owned()).unwrap(), "b".to_owned());
}
