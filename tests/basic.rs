extern crate transformation_pipeline;

use std::clone::Clone;
use transformation_pipeline::{TransformationStage, StageActions, StageResult, TransformationPipeline};

struct User {

    name: String,

}

impl Clone for User {

    fn clone(&self) -> Self {
        User {
            name: self.name.clone(),
        }
    }

}

struct CapitializeName {}

impl TransformationStage<User> for CapitializeName {

    fn run(&self, previous: User) -> StageResult<User> {

        let mut name: Vec<char> = previous.name.chars().collect();
        name[0] = name[0].to_uppercase().nth(0).unwrap();

        Ok(StageActions::Next(User {
            name: name.into_iter().collect(),
        }))
    }
}

/*
I normally don't assume that all individuals are male.  This is just a test.
*/
struct ImposeMr {}

impl TransformationStage<User> for ImposeMr {

    fn run(&self, previous: User) -> StageResult<User> {

        let mut name: String = "Mr. ".to_owned();
        name.push_str(&previous.name);

        Ok(StageActions::Next(User {
            name: name,
        }))
    }
}

#[test]
fn run_pipeline() {
    let pipeline: TransformationPipeline<User> = TransformationPipeline::new(vec![
        Box::new(CapitializeName {}),
        Box::new(ImposeMr {}),
    ]);

    assert_eq!(pipeline.run(User { name: "john".to_owned() }).unwrap().name, "Mr. John");
    assert_eq!(pipeline.run(User { name: "John Doe".to_owned() }).unwrap().name, "Mr. John Doe");
}
