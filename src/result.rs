use std::io;

pub enum StageActions<T> {

    // Abort the entire pipeline (returning an error).
    Abort,

    // Stop the current stage, and call the next stage with the input data.
    Skip,

    // Continue to the next pipeline stage, passing the given data.
    Next(T),

    // Jump (x) number of pipeline stages in the future, passing the given data.
    Jump(u8, T),

    // Finish the entire pipeline early with the given data.
    Finish(T),
}

pub type StageResult<T> = Result<StageActions<T>, io::Error>;
