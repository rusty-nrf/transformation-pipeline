use result::StageResult;

pub trait TransformationStage<T> {

    fn run(&self, previous: T) -> StageResult<T>;

}
